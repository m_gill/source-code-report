#!/usr/bin/env node
/* eslint-disable no-console */

"use strict";

var statusReport = require("./status-report");
var cleanOldReports = require("./clean-old-reports");
var persistStatusReport = require("./persist-status-report");

statusReport()
  .then(report => {
    if (!report) return;
    return persistStatusReport(report);
  })
  .then(iid => {
    if (!iid) return;
    return cleanOldReports(iid);
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  })
  .then(() => {
    process.exit(0);
  });
