/* eslint-disable no-console */
"use strict";

var fetch = require("./fetch");

let gitlabToken = process.env.GITLAB_TOKEN;
let opsGitlabToken = process.env.OPS_GITLAB_TOKEN;

function getTokenForAddress(address) {
  if (address.startsWith("https://gitlab.com/")) return gitlabToken;
  if (address.startsWith("https://ops.gitlab.net/")) return opsGitlabToken;
  return "";
}

async function get(address, method, body) {
  console.error(`# ${method || "GET"} ${address}`);

  let options = {
    method: method || "GET",
    compress: true,
    headers: {
      "Content-Type": "application/json",
      "Private-Token": getTokenForAddress(address),
      Accept: "application/json"
    }
  };

  if (body) {
    options.body = JSON.stringify(body);
  }

  let res = await fetch(address, options);
  console.error(`# ${res.status} ${res.statusText}`);

  if (!res.ok && res.status !== 304) {
    let errorMessage = res.statusText;
    try {
      let errorResponse = await res.json();
      if (errorResponse && errorResponse.message && errorResponse.message.error) {
        errorMessage = errorResponse.message.error;
      }
    } catch (e) {
      // Can't decode json response. Ignore
    }

    throw new Error(errorMessage);
  }

  let responseBody = await res.json();
  return [responseBody, res];
}

module.exports = get;
