"use strict";

var nodeFetch = require("node-fetch");
var Promise = require("bluebird");
nodeFetch.Promise = Promise;

const fetch = require("make-fetch-happen").defaults({
  cacheManager: __dirname + "/cache",
  retry: {
    retries: 3,
    randomize: true
  }
});

module.exports = fetch;
