# source-code-report

See https://gitlab.com/gl-retrospectives/create-stage/source-code/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Status%20Report for report


## What this does

1. Generate a "Status Report" for the week from Saturday -> Sunday of each week.

## How to use

1. Update the `config.json` with an array of the teams usernames
2. Update `assignees` in `config.json` with an array of the user IDs that should be assigned the generated issue
3. Update the `prefix` in `config.json` with the prefixed title of the issue for this to generate
4. Update the `project` in `config.json` with the `path/to/project` for where this issue should be generated

Run `node status.js` with the your `GITLAB_TOKEN`

## What will happen

An issue will be generated with three sections:

1. Closed issues. This includes the weight of the issue, usernames assigned to the issue, a link to the issue, the issue title, the relevant milestone if applicable, and a notation if this issue was a ~"Deliverable"
2. Merged merge requests. The username and merge request information for anything merged within the time period.
3. Reviewed merge requests. The username and merge request information for anything marked as reviewed within the time period.

