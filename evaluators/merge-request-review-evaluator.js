"use strict";

class MergeRequestReviewsEvaluator {
  constructor(since, team) {
    this.since = since;
    this.team = team;

    // A map of who in the team should get review credit for a given merge request
    this.reviewCreditMap = new Map();
    this.memberReviewCredits = new Map();
  }

  visitEventForUser(username, event) {
    if (event.action_name === "commented on" && event.note && event.note.noteable_type === "MergeRequest") {
      let ref = event.project_id && event.note.noteable_iid;

      let reviewers = this.reviewCreditMap.get(ref);
      if (!reviewers) {
        reviewers = new Set();
        this.reviewCreditMap.set(ref, reviewers);
      }

      // Credit the user with review
      reviewers.add(username);
    }
  }

  visitMergeRequest(mergeRequest) {
    // Short-circuit if not yet merged...
    if (!mergeRequest.merged_at || new Date(mergeRequest.merged_at) < this.since) return;

    // Does anyone on the team get credit for this?
    let ref = `${mergeRequest.project_id}:${mergeRequest.iid}`;
    let reviewers = this.reviewCreditMap.get(ref);

    if (reviewers) {
      // Credit each reviewer
      for (let reviewer of reviewers) {
        if (reviewer !== mergeRequest.author.username) {
          this.addCredit(reviewer, mergeRequest);
        }
      }
    }

    // Also give the merger credit, PROVIDED they are NOT also the author
    if (mergeRequest.merged_by) {
      if (this.team.has(mergeRequest.merged_by.username) && mergeRequest.merged_by.username != mergeRequest.author.username) {
        this.addCredit(mergeRequest.merged_by.username, mergeRequest);
      }
    }
  }

  visitIssue() {}

  addCredit(username, mergeRequest) {
    let credits = this.memberReviewCredits.get(username);
    let ref = `${mergeRequest.project_id}:${mergeRequest.iid}`;

    if (!credits) {
      credits = new Map();
      this.memberReviewCredits.set(username, credits);
    }

    credits.set(ref, mergeRequest);
  }

  listUsersWithReviewCredits() {
    let usernames = Array.from(this.memberReviewCredits.keys());
    return usernames;
  }

  listCreditsForUser(username) {
    let credits = this.memberReviewCredits.get(username);
    if (!credits) return [];

    return Array.from(credits.values());
  }

  render() {
    let usernames = this.listUsersWithReviewCredits();

    let lines = [];
    usernames.forEach(username => {
      let mergeRequests = this.listCreditsForUser(username);
      mergeRequests.forEach(mergeRequest => {
        lines.push([username, `* \`@${username}\`: ${mergeRequest.web_url}: ${mergeRequest.title}`]);
      });
    });
    return lines;
  }
}

module.exports = MergeRequestReviewsEvaluator;
