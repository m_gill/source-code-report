/* eslint-disable no-console */
"use strict";

const collectResults = require("./collect-results");
const httpClient = require("./http-client");
const subDays = require("date-fns/sub_days");
const startOfDay = require("date-fns/start_of_day");
const format = require("date-fns/format");
const addDays = require("date-fns/add_days");
const config = require("./config.json");

async function findStatusReport(since) {
  let isoSince = format(startOfDay(subDays(since, 1)), "YYYY-MM-DD");
  let encodedProjectUri = encodeURIComponent(config.project);

  const statusReports = await collectResults(
    `https://gitlab.com/api/v4/projects/${encodedProjectUri}/issues?labels=Status+Report&order_by=created_at&sort=desc&created_after=${isoSince}`
  );
  return statusReports[0];
}

async function createNewReport(statusReport) {
  let from = format(startOfDay(statusReport.since), "YYYY-MM-DD");
  let to = format(addDays(startOfDay(statusReport.since), 7), "YYYY-MM-DD");
  let encodedProjectUri = encodeURIComponent(config.project);

  const title = `${config.prefix}${from} - ${to}`;

  let [body] = await httpClient(`https://gitlab.com/api/v4/projects/${encodedProjectUri}/issues`, "POST", {
    labels: "Status Report",
    title: title,
    description: statusReport.body,
    confidential: true,
    assignee_ids: config.assignees
  });

  return body;
}

async function updateExistingStatusReport(existing, statusReport) {
  let encodedProjectUri = encodeURIComponent(config.project);

  if (existing.description === statusReport.body) {
    console.error("# Report description matches");
    return;
  }

  let [body] = await httpClient(`https://gitlab.com/api/v4/projects/${encodedProjectUri}/issues/${existing.iid}`, "PUT", {
    description: statusReport.body
  });

  return body;
}

// Returns the IID of the status report
async function persistStatusReport(statusReport) {
  let existing = await findStatusReport(statusReport.since);
  if (existing) {
    await updateExistingStatusReport(existing, statusReport);
    return existing.iid;
  }

  let newReportIssue = await createNewReport(statusReport);
  return newReportIssue.iid;
}

module.exports = persistStatusReport;
